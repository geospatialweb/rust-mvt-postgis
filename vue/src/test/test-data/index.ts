import { Layer, MapboxStyle, MapboxStyleURL, Route, State } from '@/enums'
import { Authentication, Deckgl, Mapbox, PageNotFound, Registration } from '@/views'

const baseURL = import.meta.env.BASE_URL

export default {
  activeMapboxStyle: MapboxStyleURL.Outdoors,
  appState: {
    initialZoom: undefined,
    isMobile: false
  },
  credentials: {
    isCorrect: true,
    isValid: true,
    password: 'secretPassword',
    role: 'user',
    username: 'foo@bar.com'
  },
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [[-76.011422, 44.384362]]
      },
      properties: {
        name: 'Frontenac Arch Biosphere Office',
        description: '19 Reynolds Road, Lansdowne, ON. Open Monday to Friday 8:30am - 4:30pm'
      }
    }
  ],
  geoJsonParams: {
    columns: 'name,description,geom',
    table: 'office'
  },
  hexagonLayerData: [],
  hexagonLayerState: {
    coverage: 1,
    elevationScale: 100,
    radius: 1000,
    upperPercentile: 100
  },
  initialZoom: 10,
  initialZoomFactor: 0.9,
  jwtState: {
    jwtExpiry: 1740862041,
    jwtToken:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3NDA4NjIwNDEsImlzcyI6Imdlb3NwYXRpYWx3ZWIuY2EiLCJyb2xlcyI6InVzZXIiLCJzdWIiOiJmb29AYmFyLmNvbSJ9.ix0WE2cw8cq2XHwbEc095opHe10OXzJuGOYPZgfuBAg'
  },
  layerControllerIcons: [
    {
      id: Layer.Satellite,
      name: 'Satellite',
      src: '/assets/icons/satellite.webp',
      height: '20',
      width: '20'
    },
    {
      id: Layer.Biosphere,
      name: 'Biosphere',
      src: '/assets/icons/biosphere.webp',
      height: '16',
      width: '16'
    },
    {
      id: Layer.Office,
      name: 'Office',
      src: '/assets/icons/office.webp',
      height: '20',
      width: '18'
    },
    {
      id: Layer.Places,
      name: 'Places',
      src: '/assets/icons/places.webp',
      height: '20',
      width: '18'
    },
    {
      id: Layer.Trails,
      name: 'Trails',
      src: '/assets/icons/trails.webp',
      height: '20',
      width: '18'
    },
    {
      id: Layer.Deckgl,
      name: 'Deck.GL',
      src: '/assets/icons/deckgl.webp',
      height: '18',
      width: '18'
    }
  ],
  layerControllerLayers: [
    {
      id: Layer.Satellite,
      name: 'Satellite',
      className: 'inactive',
      isActive: false
    },
    {
      id: Layer.Biosphere,
      name: 'Biosphere',
      className: 'active',
      isActive: true
    },
    {
      id: Layer.Office,
      name: 'Office',
      className: 'inactive',
      isActive: false
    },
    {
      id: Layer.Places,
      name: 'Places',
      className: 'inactive',
      isActive: false
    },
    {
      id: Layer.Trails,
      name: 'Trails',
      className: 'inactive',
      isActive: false
    },
    {
      id: Layer.Deckgl,
      name: 'Deck.GL',
      className: 'inactive',
      isActive: false
    }
  ],
  layerVisibility: {
    biosphere: {
      isActive: true
    },
    'biosphere-border': {
      isActive: true
    },
    trails: {
      isActive: false
    }
  },
  mapboxSettings: {
    bearing: 0,
    center: { lng: -76.25, lat: 44.5 },
    maxPitch: 75,
    maxZoom: 18,
    minZoom: 2,
    pitch: 0,
    style: MapboxStyleURL.Outdoors,
    zoom: 7
  },
  mapboxStyles: {
    outdoors: {
      id: MapboxStyle.Outdoors,
      isActive: true,
      url: MapboxStyleURL.Outdoors
    },
    satellite: {
      id: MapboxStyle.Satellite,
      isActive: false,
      url: MapboxStyle.Satellite
    }
  },
  marker: {
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: [-76.011422, 44.384362]
    },
    properties: {
      name: 'Frontenac Arch Biosphere Office',
      description: '19 Reynolds Road, Lansdowne, ON. Open Monday to Friday 8:30am - 4:30pm'
    }
  },
  markersHashmap: [
    {
      key: Layer.Office,
      value: 0
    },
    {
      key: Layer.Places,
      value: 1
    },
    {
      key: Layer.Trails,
      value: 2
    }
  ],
  markersReverseHashmap: [
    {
      key: 0,
      value: Layer.Office
    },
    {
      key: 1,
      value: Layer.Places
    },
    {
      key: 2,
      value: Layer.Trails
    }
  ],
  modalState: {
    isActive: false
  },
  routes: [
    {
      path: baseURL,
      redirect: Route.Login
    },
    {
      path: `${baseURL}${Route.Login}`,
      name: Route.Login,
      component: Authentication
    },
    {
      path: `${baseURL}${Route.Register}`,
      name: Route.Register,
      component: Registration
    },
    {
      path: `${baseURL}${Route.Deckgl}`,
      name: Route.Deckgl,
      component: Deckgl
    },
    {
      path: `${baseURL}${Route.Mapbox}`,
      name: Route.Mapbox,
      component: Mapbox
    },
    {
      path: `${baseURL}:pathMatch(.*)*`,
      name: Route.PageNotFound,
      component: PageNotFound
    }
  ],
  sliderLabelsState: {
    coverage: false,
    elevationScale: false,
    radius: false,
    upperPercentile: false
  },
  sliderValues: ['0.5', '0', '5000', '80'],
  store: {
    id: State.Modal,
    state: {
      isActive: false
    }
  },
  trailParams: {
    name: 'Blue Mountain',
    center: [-76.04, 44.508],
    zoom: 10
  }
}
