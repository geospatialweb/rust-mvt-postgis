import { Container } from 'typedi'

import { MapboxStyleService } from '@/services'
import { testData } from '@/test'

import type { IMapboxStyleState } from '@/interfaces'

describe('MapboxStyleService test suite', (): void => {
  const mapboxStyleService = Container.get(MapboxStyleService)

  test('activeMapboxStyle getter should be called with a return', (): void => {
    const { activeMapboxStyle } = testData,
      spy = vi.spyOn(mapboxStyleService, 'activeMapboxStyle', 'get').mockReturnValue(activeMapboxStyle)
    mapboxStyleService.activeMapboxStyle
    expect(spy).toBeCalled()
    expect(spy).toHaveReturned()
  })

  test('mapboxStyleState getter should be called with a return', (): void => {
    const { mapboxStyles } = testData as { mapboxStyles: IMapboxStyleState },
      spy = vi.spyOn(mapboxStyleService, 'mapboxStyleState', 'get').mockReturnValue(mapboxStyles)
    mapboxStyleService.mapboxStyleState
    expect(spy).toBeCalled()
    expect(spy).toHaveReturned()
  })

  test('setActiveMapboxStyle method should be called', (): void => {
    const spy = vi.spyOn(mapboxStyleService, 'setActiveMapboxStyle')
    mapboxStyleService.setActiveMapboxStyle()
    expect(spy).toBeCalled()
  })

  test('setMapboxStyleState method should be called', (): void => {
    const spy = vi.spyOn(mapboxStyleService, 'setMapboxStyleState')
    mapboxStyleService.setMapboxStyleState()
    expect(spy).toBeCalled()
  })
})
