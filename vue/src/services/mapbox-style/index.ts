import { Container, Service } from 'typedi'

import { State } from '@/enums'
import { StoreService } from '@/services'

import type { IMapboxStyle, IMapboxStyleState } from '@/interfaces'

@Service()
export default class MapboxStyleService {
  #activeMapboxStyle = ''

  constructor() {
    this.setActiveMapboxStyle()
  }

  get activeMapboxStyle(): string {
    return this.#activeMapboxStyle
  }

  get mapboxStyleState(): IMapboxStyleState {
    const { getState } = Container.get(StoreService)
    return <IMapboxStyleState>getState(State.MapboxStyle)
  }

  set #mapboxStyleState(state: IMapboxStyleState) {
    const { setState } = Container.get(StoreService)
    setState(State.MapboxStyle, state)
  }

  setActiveMapboxStyle = (): void => {
    const isActive = ({ isActive }: IMapboxStyle): boolean => isActive,
      { url: style } = <IMapboxStyle>Object.values(this.mapboxStyleState).find(isActive)
    this.#activeMapboxStyle = style
  }

  setMapboxStyleState = (): void => {
    const mapboxStyleState = <IMapboxStyleState>{ ...this.mapboxStyleState },
      isActive = (mapboxStyle: IMapboxStyle): boolean => (mapboxStyle.isActive = !mapboxStyle.isActive)
    Object.values(mapboxStyleState).forEach(isActive)
    this.#mapboxStyleState = mapboxStyleState
  }
}
