import { Container } from 'typedi'

import { ApiService, AuthenticationService, CredentialsService } from '@/services'
import { testData } from '@/test'

import type { ICredentialsState } from '@/interfaces'

describe('AuthenticationService test suite', (): void => {
  const { credentials } = testData as { credentials: ICredentialsState }

  beforeEach(async (): Promise<void> => {
    const { register } = Container.get(CredentialsService)
    await register(credentials)
  })

  afterEach(async (): Promise<void> => {
    const { jwtToken } = testData.jwtState,
      { deleteUser } = Container.get(ApiService)
    await deleteUser(credentials, jwtToken)
  })

  test('login method should be called', async (): Promise<void> => {
    const authenticationService = Container.get(AuthenticationService),
      spy = vi.spyOn(authenticationService, 'login')
    await authenticationService.login(credentials)
    expect(spy).toBeCalled()
    expect(spy).toBeCalledWith(credentials)
  })
})
