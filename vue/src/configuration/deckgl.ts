import { Layer, MapboxStyleURL } from '@/enums'

export default {
  options: {
    canvas: Layer.Hexagon,
    container: 'deckgl',
    controller: true,
    id: Layer.Hexagon,
    interactive: false,
    style: MapboxStyleURL.Dark
  },
  settings: {
    bearing: -30,
    center: { lng: -3.0, lat: 53.0 },
    latitude: 53.0,
    longitude: -3.0,
    maxPitch: 75,
    maxZoom: 12,
    minZoom: 4,
    pitch: 50,
    zoom: 4.5
  }
}
