import { Layer, LayerType, MapboxStyle, MapboxStyleURL } from '@/enums'

export default {
  navigationControl: {
    position: 'top-right',
    visualizePitch: true
  },
  options: {
    container: 'mapbox',
    doubleClickZoom: false
  },
  settings: {
    bearing: 0,
    center: { lng: -76.25, lat: 44.5 },
    maxPitch: 75,
    maxZoom: 18,
    minZoom: 2,
    pitch: 0,
    style: MapboxStyleURL.Outdoors,
    zoom: 7
  },
  skyLayer: {
    id: Layer.Sky,
    type: LayerType.Sky,
    paint: {
      'sky-type': 'atmosphere',
      'sky-atmosphere-sun': [0.0, 0.0],
      'sky-atmosphere-sun-intensity': 25
    }
  },
  styles: {
    outdoors: {
      id: MapboxStyle.Outdoors,
      isActive: true,
      url: MapboxStyleURL.Outdoors
    },
    satellite: {
      id: MapboxStyle.Satellite,
      isActive: false,
      url: MapboxStyleURL.Satellite
    }
  }
}
