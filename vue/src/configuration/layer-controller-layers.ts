import { Layer } from '@/enums'

export default [
  {
    id: Layer.Satellite,
    name: 'Satellite',
    isActive: false
  },
  {
    id: Layer.Biosphere,
    name: 'Biosphere',
    isActive: true
  },
  {
    id: Layer.Office,
    name: 'Office',
    isActive: false
  },
  {
    id: Layer.Places,
    name: 'Places',
    isActive: false
  },
  {
    id: Layer.Trails,
    name: 'Trails',
    isActive: false
  },
  {
    id: Layer.Deckgl,
    name: 'Deck.GL',
    isActive: false
  }
]
